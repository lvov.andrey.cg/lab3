﻿using System;

namespace QuadEquation
{
    public class QuadraticEquation
    {
        private static readonly double eps = 1e-5;
        public static double[] solve(double a, double b, double c) {

            if (Math.Abs(a) < eps)
            {
                throw new ArgumentException("First argument mustn't be 0");
            }

            double d = Math.Pow(b, 2) - 4 * a * c;

            if (Math.Abs(d) < eps)
            {
                double result = -b / (2 * a);
                return new double[2] {result, result};

            } else if (d > eps)
            {
                return new double[2] {
                    (-b + Math.Sqrt(d)) / (2 * a), 
                    (-b - Math.Sqrt(d)) / (2 * a)};
            }
    
            return new double[0];
        } 
    }

}
