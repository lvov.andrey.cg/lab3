using System;
using Xunit;
using QuadEquation;

namespace QuadEquationTest
{
    public class UnitTest1
    {

        [Fact]
        public void TestOneRootOfEquation()
        {
            double[] result = QuadraticEquation.solve(1.0, -2.0, 1.0);
            
            Assert.True(EqualDouble(1, result[0]));
            Assert.True(EqualDouble(1, result[1]));
            
        }

        [Fact]
        public void TestQuadraticEquation_1_0_Minus1_MustHaveTwoRoots()
        {
            double[] result = QuadraticEquation.solve(1, 0, -1);

            Array.Sort(result);
            Assert.True(EqualDouble(-1, result[0]));
            Assert.True(EqualDouble(1, result[1]));

        }

        [Fact]
        public void TestQuadraticEquation_1_0_1_MustNotHaveRoots()
        {
            double[] result = QuadraticEquation.solve(1, 0, 1);

            Assert.Equal(new double[0], result);
        }

        [Fact]
        public void TestQuadraticEquation_0_1_1_MustThrowExeption()
        {
            Exception ex = Assert.Throws<ArgumentException>(() => QuadraticEquation.solve(1e-6, 1, 1));
            Assert.Equal("First argument mustn't be 0", ex.Message);
        }

        private bool EqualDouble(double a, double b, double eps = 1e-5)
        {
            return Math.Abs(a - b) < eps;
        }
    }
}
